So I'm out of time... :(

I have to admit I completely overestimated how much I could get done in 4 hours. My time was spent with the following:
1. Spent 1 hour creating a mockup of the front end and creating a full set of feature files. All material from this is within the /planning folder.
2. Spent 20-30 minutes deciding what technology to use.
3. Spent remaining time (approx 4 hours in total) creating a back end API.

If you can, I suggest opening the balsamiq file within mockups. You can grab the chrome extension for this. I also provided an interactive PDF incase this was not possible. (You can click the buttons within the pdf, cool right?)

##Where I am up to
I currently have a running API backend that reflects the requirements within my feature files so far.
To view the API documentation, launch and go to http://localhost:8080/swagger-ui.html#/ 
I have also added a Postman collection within the project root "Spydr.postman_collection.json". If you run this it will give nice examples of creating both users and bugs with assigned users.

If I were to continue, my TODO list looked something like the following:
1. Create PUT endpoint to allow the modification of Bugs (asignee, status etc)
2. Create PUT endpoint to allow the modification of Users (name)
3. Initialise angular-cli app and begin front end work (Dashboard first I recon...)

##My methodology
Initially, it was tempting to take a hackathon starter from somewhere and just dig in, but I decided, after a bit of thinking, that I was going to attempt all code from scratch.
In hindsight maybe a starter kit would have been a viable option. I don't use them much so I didn't really have any that I knew and trusted.
The choice for Spring boot was two-fold; because of the small time frame, I needed to choose a technology that allowed me to hit the ground running. I did consider NodeJS, but I am still more confident with TDD in Java. I also wanted to experiment with the latest Spring Boot as I heard really good things about it. 

##How to run
###Run with maven
```
mvn spring-boot:run
```
###To run without maven installed (not tested)
```
./mvnw spring-boot:run
```
