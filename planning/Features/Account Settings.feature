Feature: Account Settings
  So that I can change information about my user
  As a user
  I want to manage my user information via a settings page

  Scenario: Viewing account settings modal
    Given a user in on the dashboard
    When they click the 'account settings' button
    Then they will be presented with the 'Account settings' modal

  Scenario: Changing a user's name
    Given a user is viewing their 'Account settings' modal
    And they modify their name
    When they click 'Save'
    Then their name will change to the new name