Feature: Bug Details
  So that I can manage bugs easier and have a better user expertience
  As a user
  I want a dashboard page to provide data overviews and shortcuts to actions

  Scenario: Viewing bug details
    Given a user is on the Dashboard page
    When they click a bug within the 'Open Bugs' widget
    Then they will see the status, asignee, title, description and 'opened on' timestamp

  Scenario: Changing bug asignee
    Given a user is on the Bug Details page
    And they have changed the asignee of the bug to a different user
    When they click 'Save'
    Then the bug's asignee should change to the selected asignee
    And the user will be taken back to the dashboard

  Scenario: Closing a bug
    Given a user is on the Bug Details page
    And the bug status is set to 'Open'
    When they change the status to 'Closed'
    And they click 'Save'
    Then the bug will be closed
    And the user will be taken back to the dashboard
	  