Feature: Dashboard
  So that I can manage bugs easier and have a better user expertience
  As a user
  I want a dashboard page to provide data overviews and shortcuts to actions

  Scenario: Dashboard - Show 'Create Bug' modal
    Given a user is on the 'Bugs' page
    When they click the 'Create bug' button
    Then a 'Create Bug' modal will appear

  Scenario: Dashboard - Create a new bug
    Given A user is presented with the 'Create Bug' modal
    And they have entered a valid title, description and asignee
    When They click 'Create'
    Then The bug should be created with a correct 'opened on' timestamp

  Scenario: Dashboard - View all open bugs
    Given a user is on the dashboard
    Then they should see a list of all open bugs within the 'Open Bugs' widget
    And the titles of each bug should be visible

  Scenario: Dashboard - View bug details
    Given a user is on the dashboard
    When they click on any bug within the 'Open Bugs' widget
    Then they should see the 'Bug details' modal appear
    And the modal should contain the title, description, asignee and 'opened on' time
	  
  