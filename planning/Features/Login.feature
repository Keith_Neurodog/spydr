Feature: Login
  So that I can manage bugs
  As a user
  I want to login to the system with a simple first/last name

  Scenario: Successful Login
    Given a user is on the Login page
    And they have entered a valid first and last name
    When they click the Login button
    Then they will be presented with the Dashboard page

  Scenario: Unsuccessful Login
    Given a user is on the Login page
    And they have entered an invalid first name
    When they click the Login button
    Then they will be presented with the Dashboard page