Feature: User Management
  So that a new user can manage bugs
  As a user
  I want to manage users within the system

  Scenario: Access the 'Users' page
    Given a user is on the Dashboard page
    When they click the Users button
    Then they will be presented with the 'Users' page

  Scenario: Opening the 'Add User' modal
    Given a user in on the 'Users' page
    When They click 'Add new user'
    Then they will see the 'Add User' modal

  Scenario: Adding a new user
    Given a user has input a first and last name within the 'Add User' modal
    When They click 'Add User'
    Then the user will be created