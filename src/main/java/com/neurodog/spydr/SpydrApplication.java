package com.neurodog.spydr;

import com.neurodog.spydr.entities.Bug;
import com.neurodog.spydr.entities.BugStatus;
import com.neurodog.spydr.entities.User;
import com.neurodog.spydr.repositories.BugRepository;
import com.neurodog.spydr.repositories.UserRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.util.stream.Stream;

@SpringBootApplication
public class SpydrApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpydrApplication.class, args);
    }

    /**
     * Seed some data into the DB because we are running an in memory H2 DB right now.
     *
     * @param userRepository the user repo
     * @param bugRepository  the bug repo
     * @return a runnable bean for spring to init the users
     */
    @Bean
    @Profile("!test")
    ApplicationRunner init(UserRepository userRepository, BugRepository bugRepository) {
        return args -> {
            Stream.of("John Doe", "Keith Hayes", "Dave").forEach(name -> {
                User e = new User();
                e.setName(name);
                userRepository.save(e);
            });
            userRepository.findAll().forEach(System.out::println);

            bugRepository.save(new Bug("A super bad bug", "Oh no it's IE related!", BugStatus.OPEN, userRepository.findAll().get(0)));
            bugRepository.save(new Bug("It all broke again...", "TODO: Pop some steps in later", BugStatus.OPEN, userRepository.findAll().get(0)));
            bugRepository.save(new Bug("A minor bug", "Probably ignore this, it's not a real problem", BugStatus.OPEN, userRepository.findAll().get(1)));
            bugRepository.save(new Bug("A closed bug", "Shouldn't see this bug anymore because it's closed!", BugStatus.CLOSED, userRepository.findAll().get(1)));
            bugRepository.findAll().forEach(System.out::println);
        };
    }
}
