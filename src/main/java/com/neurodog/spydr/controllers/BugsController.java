package com.neurodog.spydr.controllers;

import com.neurodog.spydr.dtos.BugRequestDto;
import com.neurodog.spydr.entities.Bug;
import com.neurodog.spydr.repositories.BugRepository;
import com.neurodog.spydr.services.BugService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Transactional(readOnly = true)
@Api
public class BugsController {

    private BugRepository bugRepository;
    private BugService bugService;

    @Autowired
    public BugsController(BugRepository bugRepository, BugService bugService) {
        this.bugRepository = bugRepository;
        this.bugService = bugService;
    }

    @RequestMapping("/bugs")
    public List<Bug> getBugs() {
        return bugRepository.findAll();
    }

    @RequestMapping("/bug/{id}")
    public Bug getBug(@PathVariable Long id) {
        Optional<Bug> byId = bugRepository.findById(id);

        if (byId.isPresent())
            return byId.get();
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Bug %s Not Found", id));
    }

    @RequestMapping(value = "/bugs", method = RequestMethod.POST)
    public Bug postBugs(@RequestBody BugRequestDto req) {
        return bugService.createBug(req);
    }
}
