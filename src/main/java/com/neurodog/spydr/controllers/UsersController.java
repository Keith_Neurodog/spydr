package com.neurodog.spydr.controllers;

import com.neurodog.spydr.entities.User;
import com.neurodog.spydr.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Transactional(readOnly = true)
public class UsersController {

    private UserRepository userRepository;

    @Autowired
    public UsersController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping("/users")
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @RequestMapping("/user/{id}")
    public User getUser(@PathVariable Long id) {
        Optional<User> byId = userRepository.findById(id);

        if (byId.isPresent())
            return byId.get();
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User %s Not Found", id));
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public User postUsers(@RequestBody String name) {
        return userRepository.save(new User(name));
    }
}
