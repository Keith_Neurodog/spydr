package com.neurodog.spydr.dtos;

public class BugRequestDto {
    String title;
    String description;
    String status;
    Long asigneeId;

    public BugRequestDto() {
    }

    public BugRequestDto(String title, String description, String status, Long asigneeId) {
        this.title = title;
        this.description = description;
        this.status = status;
        this.asigneeId = asigneeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getAsigneeId() {
        return asigneeId;
    }

    public void setAsigneeId(Long asigneeId) {
        this.asigneeId = asigneeId;
    }
}
