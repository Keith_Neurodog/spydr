package com.neurodog.spydr.entities;

import org.springframework.lang.NonNull;

import javax.persistence.*;

@Entity
public class Bug {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NonNull
    private String title;

    @NonNull
    private String description;

    @NonNull
    @Enumerated(EnumType.STRING)
    private BugStatus status;

    @ManyToOne
    @JoinColumn
    private User asignee;

    public Bug() {
    }

    public Bug(String title, String description, BugStatus status) {
        this.title = title;
        this.description = description;
        this.status = status;
    }

    public Bug(String title, String description, BugStatus status, User asignee) {
        this.title = title;
        this.description = description;
        this.status = status;
        this.asignee = asignee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BugStatus getStatus() {
        return status;
    }

    public void setStatus(BugStatus status) {
        this.status = status;
    }

    public User getAsignee() {
        return asignee;
    }

    public void setAsignee(User asignee) {
        this.asignee = asignee;
    }
}
