package com.neurodog.spydr.entities;

public enum BugStatus {
    OPEN,
    CLOSED
}
