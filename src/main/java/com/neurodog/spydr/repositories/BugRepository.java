package com.neurodog.spydr.repositories;

import com.neurodog.spydr.entities.Bug;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BugRepository extends JpaRepository<Bug, Long> {
}
