package com.neurodog.spydr.repositories;

import com.neurodog.spydr.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
