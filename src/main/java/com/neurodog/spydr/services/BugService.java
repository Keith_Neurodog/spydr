package com.neurodog.spydr.services;

import com.neurodog.spydr.dtos.BugRequestDto;
import com.neurodog.spydr.entities.Bug;
import org.springframework.stereotype.Service;

@Service
public interface BugService {
    Bug createBug(BugRequestDto bugRequest);
}
