package com.neurodog.spydr.services;

import com.neurodog.spydr.dtos.BugRequestDto;
import com.neurodog.spydr.entities.Bug;
import com.neurodog.spydr.entities.BugStatus;
import com.neurodog.spydr.entities.User;
import com.neurodog.spydr.repositories.BugRepository;
import com.neurodog.spydr.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class BugServiceImpl implements BugService {
    private UserRepository userRepository;
    private BugRepository bugRepository;

    @Autowired
    public BugServiceImpl(UserRepository userRepository, BugRepository bugRepository) {
        this.userRepository = userRepository;
        this.bugRepository = bugRepository;
    }

    @Override
    public Bug createBug(BugRequestDto requestDto) {
        Long asigneeId = requestDto.getAsigneeId();
        Bug newBug = new Bug(requestDto.getTitle(), requestDto.getDescription(), BugStatus.valueOf(requestDto.getStatus()));

        if (asigneeId != null) {
            Optional<User> byId = userRepository.findById(asigneeId);

            if (byId.isPresent())
                newBug.setAsignee(byId.get());
            else
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User %s Not Found", requestDto.getAsigneeId()));
        }

        return bugRepository.save(newBug);
    }
}
