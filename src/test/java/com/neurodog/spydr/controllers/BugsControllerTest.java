package com.neurodog.spydr.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neurodog.spydr.dtos.BugRequestDto;
import com.neurodog.spydr.entities.Bug;
import com.neurodog.spydr.entities.BugStatus;
import com.neurodog.spydr.entities.User;
import com.neurodog.spydr.repositories.BugRepository;
import com.neurodog.spydr.services.BugService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(BugsController.class)
@ActiveProfiles("test")
public class BugsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BugRepository bugRepository;

    @MockBean
    private BugService bugService;

    @Test
    public void getBug_shouldReturnBugIfPassedId() throws Exception {
        given(bugRepository.findById(aValidBugId())).willReturn(Optional.of(aValidBug()));

        mvc.perform(get("/api/bug/" + aValidBugId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(aValidBug().getTitle())))
                .andExpect(jsonPath("$.description", is(aValidBug().getDescription())))
                .andExpect(jsonPath("$.status", is(aValidBug().getStatus().toString())))
                .andExpect(jsonPath("$.asignee.id", is(aValidBug().getAsignee().getId().intValue())));
    }

    @Test
    public void getBug_shouldReturn404StatusIfPassedInvalidId() throws Exception {
        given(bugRepository.findById(anyLong())).willReturn(Optional.empty()); // unknown id

        mvc.perform(get("/api/bug/123")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }


    @Test
    public void getBugs_shouldReturnAllBugs() throws Exception {
        Bug bug1 = aValidBug();
        bug1.setTitle("bug 1 title");
        Bug bug2 = aValidBug();
        bug2.setTitle("bug 2 title");
        List<Bug> allBugs = Arrays.asList(bug1, bug2);

        given(bugRepository.findAll()).willReturn(allBugs);

        mvc.perform(get("/api/bugs")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].title", is(bug1.getTitle())))
                .andExpect(jsonPath("$[1].title", is(bug2.getTitle())));
    }

    @Test
    public void postBugs_shouldDelegateToBugService() throws Exception {
        given(bugService.createBug(any(BugRequestDto.class))).willReturn(aValidBug());

        ObjectMapper objectMapper = new ObjectMapper();
        BugRequestDto br = aValidBugRequest();

        mvc.perform(post("/api/bugs")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(br))) // post the name as the body
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(aValidBug().getTitle())));
    }

    private BugRequestDto aValidBugRequest() {
        return new BugRequestDto("a title", "a desc", "OPEN", 1L);
    }

    private Bug aValidBug() {
        Bug bug = new Bug("a title", "a description", BugStatus.OPEN, aValidUser());
        bug.setId(aValidBugId());
        return bug;
    }

    private User aValidUser() {
        User user = new User("a name");
        user.setId(aValidUserId());
        return user;
    }

    private Long aValidBugId() {
        return 123L;
    }

    private Long aValidUserId() {
        return 414L;
    }
}