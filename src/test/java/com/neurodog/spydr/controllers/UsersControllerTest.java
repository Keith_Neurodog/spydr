package com.neurodog.spydr.controllers;

import com.neurodog.spydr.entities.User;
import com.neurodog.spydr.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UsersController.class)
@ActiveProfiles("test")
public class UsersControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository userRepository;

    @Test
    public void getUser_shouldReturnUserIfPassedId() throws Exception {
        User thomas = new User("Thomas");
        long id = 123L;
        given(userRepository.findById(id)).willReturn(Optional.of(thomas));

        mvc.perform(get("/api/user/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(thomas.getName())));
    }

    @Test
    public void getUser_shouldReturn404StatusIfPassedInvalidId() throws Exception {
        given(userRepository.findById(anyLong())).willReturn(Optional.empty());

        mvc.perform(get("/api/user/123")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getUsers_shouldReturnAllUsers() throws Exception {
        User thomas = new User("Thomas");
        User rupert = new User("Rupert");
        List<User> allUsers = Arrays.asList(thomas, rupert);

        given(userRepository.findAll()).willReturn(allUsers);

        mvc.perform(get("/api/users")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(thomas.getName())))
                .andExpect(jsonPath("$[1].name", is(rupert.getName())));
    }

    @Test
    public void postUsers_shouldSaveAndReturnUser() throws Exception {
        User thomas = new User("Thomas");
        given(userRepository.save(any(User.class))).willReturn(thomas);

        mvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(thomas.getName())) // post the name as the body
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(thomas.getName())));
    }
}