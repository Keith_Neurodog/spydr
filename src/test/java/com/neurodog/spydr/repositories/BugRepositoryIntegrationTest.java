package com.neurodog.spydr.repositories;

import com.neurodog.spydr.SpydrApplication;
import com.neurodog.spydr.entities.Bug;
import com.neurodog.spydr.entities.BugStatus;
import com.neurodog.spydr.entities.User;
import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpydrApplication.class)
public class BugRepositoryIntegrationTest {

    @Autowired
    private BugRepository bugRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    @Transactional
    @Ignore
    public void bugRepository_shouldCreateDefaultBugs() {
        List<Bug> all = bugRepository.findAll();

        Assertions.assertThat(all)
                .hasSize(4)
                .extracting(Bug::getTitle)
                .containsExactlyInAnyOrder("A super bad bug", "It all broke again...", "A minor bug", "A closed bug");
    }

    @Test
    @Transactional
    @Ignore
    public void bugRepository_shouldSaveAndRetrieveAdditionalBugs() {
        User aUser = userRepository.findAll().get(0);
        bugRepository.save(new Bug("A new bug!", "A tiny one", BugStatus.OPEN, aUser));
        List<Bug> all = bugRepository.findAll();

        Assertions.assertThat(all)
                .hasSize(5)
                .extracting(Bug::getTitle)
                .containsExactlyInAnyOrder("A super bad bug", "It all broke again...", "A minor bug", "A closed bug", "A new bug!");
    }
}
