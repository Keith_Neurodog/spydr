package com.neurodog.spydr.repositories;

import com.neurodog.spydr.SpydrApplication;
import com.neurodog.spydr.entities.User;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpydrApplication.class)
public class UserRepositoryIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @Transactional
    public void userRepository_shouldCreateDefaultUsers() {
        List<User> all = userRepository.findAll();

        Assertions.assertThat(all)
                .hasSize(3)
                .extracting(User::getName)
                .containsExactlyInAnyOrder("John Doe", "Keith Hayes", "Dave");
    }

    @Test
    @Transactional
    public void userRepository_shouldSaveAndRetrieveAdditionalUsers() {
        userRepository.save(new User("Lewis"));
        userRepository.save(new User("John"));
        List<User> all = userRepository.findAll();

        Assertions.assertThat(all)
                .hasSize(5)
                .extracting(User::getName)
                .containsExactlyInAnyOrder("John Doe", "Keith Hayes", "Dave", "Lewis", "John");
    }
}
