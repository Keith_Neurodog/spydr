package com.neurodog.spydr.services;

import com.neurodog.spydr.dtos.BugRequestDto;
import com.neurodog.spydr.entities.Bug;
import com.neurodog.spydr.entities.User;
import com.neurodog.spydr.repositories.BugRepository;
import com.neurodog.spydr.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class BugServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private BugRepository bugRepository;

    @InjectMocks
    private BugServiceImpl bugService;

    @Captor
    private ArgumentCaptor<Bug> bugCaptor;

    @Test
    public void createBug_shouldObtainUserFromUserRepo_WhenPassedAUserId() {
        given(userRepository.findById(aValidUserId())).willReturn(Optional.of(aValidUser()));

        BugRequestDto validBugRequest = aValidBugRequest();
        validBugRequest.setAsigneeId(aValidUserId()); // needs to find this user

        bugService.createBug(validBugRequest);

        verify(userRepository).findById(validBugRequest.getAsigneeId());
    }

    @Test
    public void createBug_should_NOT_ObtainUserFromUserRepo_WhenPassedNoUserId() {
        BugRequestDto validBugRequest = aValidBugRequest();
        validBugRequest.setAsigneeId(null); // this bug is unassigned - no need to find a user

        bugService.createBug(validBugRequest);

        verify(userRepository, never()).findById(validBugRequest.getAsigneeId());
    }

    @Test
    public void createBug_shouldCallUserRepoToSaveNewBug() {
        given(userRepository.findById(aValidUserId())).willReturn(Optional.of(aValidUser()));

        bugService.createBug(aValidBugRequest());

        verify(bugRepository).save(bugCaptor.capture());
        assertThat(bugCaptor.getValue().getTitle(), is(aValidBugRequest().getTitle()));
        assertThat(bugCaptor.getValue().getDescription(), is(aValidBugRequest().getDescription()));
        assertThat(bugCaptor.getValue().getStatus().toString(), is(aValidBugRequest().getStatus()));
        assertThat(bugCaptor.getValue().getAsignee().getId(), is(aValidBugRequest().getAsigneeId()));
    }

    @Test(expected = ResponseStatusException.class)
    public void createBug_shouldThrowIfUserIdWasRequestedButNotFoundWithinDB() {
        given(userRepository.findById(aValidUserId())).willReturn(Optional.empty()); // Empty optional response!
        bugService.createBug(aValidBugRequest());
    }

    private BugRequestDto aValidBugRequest() {
        return new BugRequestDto("a valid title", "a valid desc", "OPEN", aValidUserId());
    }

    private User aValidUser() {
        User bob = new User("Bob");
        bob.setId(aValidUserId());
        return bob;
    }

    private Long aValidUserId() {
        return 123L;
    }
}